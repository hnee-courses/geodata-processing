"""
cutout using a shapefile from a folder with rasters



code from https://www.earthdatascience.org/courses/use-data-open-source-python/intro-raster-data-python/raster-data-processing/crop-raster-data-with-shapefile-in-python/
"""
from pathlib import Path

import seaborn as sns

from shapely.geometry import mapping
import rioxarray as rxr
import geopandas as gpd
from loguru import logger


def clip_raster_with_aoi(raster_path, aoi, output_path = Path("./cutout_raster.tif")):
    """
    cut out a raster using the aoi

    :param raster_path:
    :param aoi:
    :return:
    """
    crop_extent = gpd.read_file(aoi)

    raster_image = rxr.open_rasterio(raster_path,
                                     masked=True).squeeze()

    raster_clipped = raster_image.rio.clip(crop_extent.geometry.apply(mapping),
                                          # This is needed if your GDF is in a diff CRS than the raster data
                                          crop_extent.crs)

    path_to_tif_file = Path(output_path)
    raster_clipped.rio.to_raster(path_to_tif_file, driver="GTiff")
    logger.info(f"wrote raster to {path_to_tif_file}")

    return path_to_tif_file


def clip_all_matching_images(aoi_path, template, raster_base_path):
    """

    :param aoi_path:
    :param template:
    :param raster_base_path:
    :return:
    """
    # raster_path =  raster_base_path.joinpath("LC08_L1TP_193023_20220515_20220515_02_RT_B1.TIF")
    aoi_path = aoi_path

    rasters = raster_base_path.glob(template)

    for raster in rasters:
        parts = list(raster.parts)
        parts[-1] = f"clip_{parts[-1]}"
        yield clip_raster_with_aoi(raster_path=raster, aoi=aoi_path, output_path=Path(*parts))



if __name__ == '__main__':
    raster_base_path = Path("F:\QGIS_projects\Geodataprocessing - Practical 2\LC08_L1TP_193023_20220515_20220515_02_RT")
    raster_base_path = Path("F:\QGIS_projects\Geodataprocessing - Practical 2\Sentinel\S2B_MSIL1C_20220307T101759_N0400_R065_T33UUU_20220307T122828.SAFE\GRANULE\L1C_T33UUU_A026117_20220307T102247\IMG_DATA")


    # raster_path =  raster_base_path.joinpath("LC08_L1TP_193023_20220515_20220515_02_RT_B1.TIF")
    aoi_path = Path("F:\QGIS_projects\Geodataprocessing - Practical 2\\aoi.shp")

    filenames = clip_all_matching_images(aoi_path = aoi_path, template = "T33UUU*.jp2", raster_base_path=raster_base_path)

    [print(filename) for filename in filenames]

    # for raster in rasters:
    #     parts = list(raster.parts)
    #     parts[-1] = f"cut_{parts[-1]}"
    #     clip_raster_with_aoi(raster_path=raster, aoi=aoi_path, output_path=Path(*parts))




