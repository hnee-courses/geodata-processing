from pathlib import Path

import rasterio
import pandas as pd
import numpy as np
from rasterio.plot import show_hist
import seaborn as sns
import matplotlib.pyplot as plt
from loguru import logger

## load the first N bands of the landssat image
base_path = Path("F:\QGIS_projects\Geodataprocessing - Practical 2\Landsat")
bands = [
    {
        "band": "1",
        "filename": base_path.joinpath("clip_LC08_L1TP_193023_20220515_20220515_02_RT_B1.TIF"),
    },
    {
        "band": "2",
        "filename": base_path.joinpath("clip_LC08_L1TP_193023_20220515_20220515_02_RT_B2.TIF"),
    },
    {
        "band": "3",
        "filename": base_path.joinpath("clip_LC08_L1TP_193023_20220515_20220515_02_RT_B3.TIF"),
    },

    {
        "band": "4",
        "filename": base_path.joinpath("clip_LC08_L1TP_193023_20220515_20220515_02_RT_B4.TIF"),
    },
    {
        "band": "5",
        "filename": base_path.joinpath("clip_LC08_L1TP_193023_20220515_20220515_02_RT_B5.TIF"),
    },
    {
        "band": "6",
        "filename": base_path.joinpath("clip_LC08_L1TP_193023_20220515_20220515_02_RT_B6.TIF"),
    }
]

band_stats = []


def dark_object_correction(filename, band_no, output_path):
    """
    shift the histogram of the image to the left, so the minimum will be 1
    :param filename:
    :param band_no:
    :param output_path:
    :return:
    """

    dataset = rasterio.open(filename)

    nd_band_i = dataset.read(1)
    profile = dataset.profile
    # print(band1)
    min = nd_band_i.min()
    max = nd_band_i.max()

    nd_band_i_flat = nd_band_i.ravel()
    plot_band_i = sns.histplot(
        data=nd_band_i_flat, kde=False, bins=1000)
    plot_band_i.set_title(f"Raster Histogram before correction for band {band_no}")
    plot_band_i.set_ylabel("Frequency", fontsize=10)
    plot_band_i.set_xlabel("Pixel Value", fontsize=10)
    fig = plot_band_i.get_figure()
    plt.show()
    fig.savefig(Path(f"./bc_black_object_band_{band_no}.png"))

    ## execute Dark-object atmospheric correction
    nd_band_i_dobject = nd_band_i - min

    nd_band_i_dobject_flat = nd_band_i_dobject.ravel()
    plot_band_ac_i = sns.histplot(
        data=nd_band_i_dobject_flat, kde=False, bins=1000)
    plot_band_ac_i.set_title(f"Raster Histogram after dark object correction for band {band_no}")
    plot_band_ac_i.set_ylabel("Frequency", fontsize=10)
    plot_band_ac_i.set_xlabel("Pixel Value", fontsize=10)
    fig = plot_band_ac_i.get_figure()
    plt.show()
    fig.savefig(Path(f"./ac_black_object_band_{band_no}.png"))
    logger.info(f"executed dark object correction for band number: {band_no}")

    path_to_tif_file = Path(output_path)
    with rasterio.open(path_to_tif_file, 'w', **profile) as dst:
        dst.write(nd_band_i_dobject.astype(rasterio.uint16), 1)
    logger.info(f"wrote raster to {path_to_tif_file}")

    return output_path


if __name__ == '__main__':

    for band_i in bands:
        band_no = band_i["band_no"]
        output_path = "abc.tif"
        dark_object_correction(band_i["filename"], band_no, output_path)

    print(band_stats)
    df_band_stats = pd.DataFrame(band_stats)
    df_band_stats = df_band_stats.T

    print(df_band_stats)
    with open("bands_statistics.csv", 'w') as f:
        logger.info(f"wrote band statistics to csv")
        df_band_stats.T.to_csv(f)