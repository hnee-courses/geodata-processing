"""
Task 2

regression adjustment

using this python tutorial: https://realpython.com/linear-regression-in-python/
"""
from pathlib import Path
import seaborn as sns

import rasterio
import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
from loguru import logger

from sklearn.linear_model import LinearRegression


base_path_ref_image = Path("F:\QGIS_projects\Geodataprocessing - Practical 2\Landsat")
base_path_sent_image = Path("F:\QGIS_projects\Geodataprocessing - Practical 2\Sentinel\S2B_MSIL1C_20220307T101759_N0400_R065_T33UUU_20220307T122828.SAFE\GRANULE\L1C_T33UUU_A026117_20220307T102247\IMG_DATA")
### bands for sentinel
band_configuration = [
    ("Blue", 2, 2),
    ("Green", 3, 3),
    ("Red", 4, 4),
    ("Near Infrared", 5, "8A"),
    ("SWIR", 6, 11)
]

bands = [
    {
        "band_desc": "blue",
        "filename": base_path_ref_image.joinpath("clip_LC08_L1TP_193023_20220515_20220515_02_RT_B2.TIF"),
        "sent_filename": base_path_sent_image.joinpath("cut_T33UUU_20220307T101759_B02.jp2"),
    },
    {
        "band_desc": "Green",
        "filename": base_path_ref_image.joinpath("clip_LC08_L1TP_193023_20220515_20220515_02_RT_B3.TIF"),
        "sent_filename": base_path_sent_image.joinpath("cut_T33UUU_20220307T101759_B03.jp2"),
    },
    {
        "band_desc": "Red",
        "filename": base_path_ref_image.joinpath("clip_LC08_L1TP_193023_20220515_20220515_02_RT_B4.TIF"),
        "sent_filename": base_path_sent_image.joinpath("cut_T33UUU_20220307T101759_B04.jp2"),
    },
    {
        "band_desc": "Near Infrared",
        "filename": base_path_ref_image.joinpath("clip_LC08_L1TP_193023_20220515_20220515_02_RT_B5.TIF"),
        "sent_filename": base_path_sent_image.joinpath("cut_T33UUU_20220307T101759_B8A.jp2"),
    },
    {
        "band_desc": "SWIR",
        "filename": base_path_ref_image.joinpath("clip_LC08_L1TP_193023_20220515_20220515_02_RT_B6.TIF"),
        "sent_filename": base_path_sent_image.joinpath("cut_T33UUU_20220307T101759_B11.jp2"),
    },

    ]

def get_min_max_pixels(band_i):
    """
    get the min/max values for the image
    :param band_i:
    :return:
    """

    # landsat
    dataset = rasterio.open(band_i["filename"])
    nd_band_i = dataset.read(1)
    nd_band_i_flat = nd_band_i.ravel()

    ## sort the array

    five_max_elements = list(nd_band_i_flat[np.argpartition(nd_band_i_flat, -5)[-5:]])
    five_min_elements = list(nd_band_i_flat[np.argpartition(nd_band_i_flat, -5)[:5]])

    band_i["landsat_five_min_elements"] = five_min_elements
    band_i["landsat_five_max_elements"] = five_max_elements

    # do the same for sentinel
    ## FIXME, actually it would be more correct, to take the same pixels
    dataset = rasterio.open(band_i["sent_filename"])
    nd_band_i = dataset.read(1)
    nd_band_i_flat = nd_band_i.ravel()

    ## sort the array
    five_max_elements = list(nd_band_i_flat[np.argpartition(nd_band_i_flat, -5)[-5:]])
    five_min_elements = list(nd_band_i_flat[np.argpartition(nd_band_i_flat, -5)[:5]])

    band_i["sentinel_five_min_elements"] = five_min_elements
    band_i["sentinel_five_max_elements"] = five_max_elements

    return band_i


def plot_the_regression(band_i):
    """

    :param band_i:
    :return:
    """
    band_desc = (band_i["band_desc"])
    ## y is landsat
    y = np.concatenate((band_i["landsat_five_min_elements"], band_i["landsat_five_max_elements"]))
    ## x is sentinel
    x = np.concatenate((band_i["sentinel_five_min_elements"], band_i["sentinel_five_max_elements"]))
    # x = np.array([5, 15, 25, 35, 45, 55]).reshape((-1, 1))
    x = x.reshape((-1, 1))
    # y = np.array([5, 20, 14, 32, 22, 38])

    reg = LinearRegression().fit(x, y)

    print(f"score: {reg.score(x, y)}")

    print(f"intercept: {reg.intercept_}")
    intercept = reg.intercept_
    print(f"slope: {reg.coef_}")
    slope = reg.coef_[0]
    print(f"predict: {reg.predict(x)}")


    logger.info(f"The model is: {round(reg.coef_[0], 3)} * x + {round(reg.intercept_, 3)}")
    df_data = pd.DataFrame({
        "sentinel": np.concatenate((band_i["sentinel_five_min_elements"], band_i["sentinel_five_max_elements"])),
        "landsat": np.concatenate((band_i["landsat_five_min_elements"], band_i["landsat_five_max_elements"]))
    }
    )

    regression_plot = sns.scatterplot(data=df_data, x="sentinel", y="landsat")
    regression_plot.set(title=f"Regression Plot for Band: {band_desc}")

    df_lineplot = pd.DataFrame({
        "x": [0, 25000],
        "y": [reg.predict([[0]])[0], reg.predict([[25000]])[0]]
    }
    )
    # regression_plot.ax.axline(xy1=(0, reg.intercept_), slope=reg.coef_[0], color="b", dashes=(5, 2))
    sns.lineplot(data=df_lineplot, x="x", y="y", linewidth=2)
    fig = regression_plot.get_figure()
    plt.show()

    output_path = Path(f"regression_plot_band_{band_desc}.png")
    fig.savefig(output_path)

    logger.info(f"saved regression to {output_path}")

    return reg

if __name__ == '__main__':

    for band_i in bands:
        band_i = get_min_max_pixels(band_i)

        reg = plot_the_regression(band_i=band_i)

        ## apply the formula on the sentinel images


        dataset = rasterio.open(band_i["sent_filename"])
        band_desc = (band_i["band_desc"])

        nd_band_i = dataset.read(1)
        nd_band_i_flat = nd_band_i.ravel()
        nd_band_i.shape
        nd_band_i_reshape = nd_band_i.reshape((-1, 1))

        plot_band_bf = sns.histplot(
            data=nd_band_i_flat, kde=False, bins=1000)
        plot_band_bf.set_title(f"Raster Histogram before Regression Adjustment for band {band_desc}")
        plot_band_bf.set_ylabel("Frequency", fontsize=10)
        plot_band_bf.set_xlabel("Pixel Value", fontsize=10)

        fig = plot_band_bf.get_figure()
        plt.show()
        fig.savefig(Path(f"./bc_regression_adjustment_band_{band_desc}.png"))


        sentinel_corrected_flat = reg.predict(nd_band_i_reshape)

        #sentinel_corrected = sentinel_corrected_flat.reshape(nd_band_i.shape)

        plot_band_ac_i = sns.histplot(
            data=sentinel_corrected_flat, kde=False, bins=1000)
        plot_band_ac_i.set_title(f"Raster Histogram after Regression Adjustment for band {band_desc}")
        plot_band_ac_i.set_xlabel("Frequency", fontsize=10)
        plot_band_ac_i.set_ylabel("Pixel Value", fontsize=10)

        fig = plot_band_ac_i.get_figure()
        plt.show()
        fig.savefig(Path(f"./ac_regression_adjustment_band_{band_desc}.png"))

        logger.info(f"executed regression_adjustment on band image {band_desc}")