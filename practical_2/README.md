# Geodata Processing Practicals

## Practical 2

### Data preparation 
clip raster image using a shapefile.
```
clip_image_by_aoi.py
```

### Task 1
Correct an Image using the method of dark object correction.
```
task_1_get_image_statistics.py
```

### Task 2
Correct an Image using the method Regression Adjustment.
```
task_2_linear_regression_on_landsat.py
```
