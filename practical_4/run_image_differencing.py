from pathlib import Path

import rasterio
from loguru import logger
import matplotlib.pyplot as plt
from rasterio.plot import show

from practical_4.histogram_matching import plot_histograms


def run_image_differencing(a_path, b_path, output_path, vis_output_path, band_no):
    """
    execute image differencing a-b

    :param a_path:
    :param b_path:
    :return:
    """
    SMALL_SIZE = 8
    MEDIUM_SIZE = 10
    BIGGER_SIZE = 12

    plt.rc('font', size=SMALL_SIZE)  # controls default text sizes
    plt.rc('axes', titlesize=SMALL_SIZE)  # fontsize of the axes title
    plt.rc('axes', labelsize=MEDIUM_SIZE)  # fontsize of the x and y labels
    plt.rc('xtick', labelsize=SMALL_SIZE)  # fontsize of the tick labels
    plt.rc('ytick', labelsize=SMALL_SIZE)  # fontsize of the tick labels
    plt.rc('legend', fontsize=SMALL_SIZE)  # legend fontsize
    plt.rc('figure', titlesize=BIGGER_SIZE)  # fontsize of the figure title

    logger.info(f"execute the image differencing")

    dataset_to_match = rasterio.open(a_path)
    image_to_match = dataset_to_match.read(1)
    profile = dataset_to_match.profile
    dataset_reference = rasterio.open(b_path)
    image_reference = dataset_reference.read(1)

    image_reference = image_reference.astype(rasterio.float64)
    image_to_match = image_to_match.astype(rasterio.float64)
    difference = image_reference - image_to_match

    path_to_tif_file = Path(output_path)
    with rasterio.open(path_to_tif_file, 'w', **profile) as dst:
        dst.write(difference.astype(rasterio.float64), 1)
    # matched.rio.to_raster(path_to_tif_file, driver="GTiff")
    logger.info(f"wrote raster to {path_to_tif_file}")

    ## TODO the difference image suffers from some problems. There are some changes ( maybe the thresholding helps )
    fig, ax = plt.subplots(1, figsize=(12, 12))
    ax.set_title(f'Differenced Image for {band_no}')
    plt.imshow(difference, cmap='Greys_r')
    plt.savefig(f"{vis_output_path}_image_differencing_{band_no}.png")
    plt.show(ax=ax)

    fig, ax = plt.subplots(1, figsize=(12, 12))
    ax.set_title(f'Contoured Differenced Image for band {band_no}')
    show(difference, cmap='Greys_r', interpolation='none', ax=ax)

    show(difference, contour=True, ax=ax)
    plt.savefig(f"{vis_output_path}_image_differencing_contours.png")
    plt.show()

    ## plot the histogram of the difference image
    plot_histograms(difference, filename=f"{vis_output_path}_image_differencing_histogram.png",
                    title=f"Histogram of Difference Image for band {band_no}", max_y=15000)
    logger.warning(f"don't forget the corner thresholding")

    ## 2021 - 2017 -
    # sth. is missing in 17 but was there in 21, value high
    # sth. is missing in 21 but was there in 17, value low, probably negative
    ## binarize the results
    threshold = difference.max() / 3 * 1 ## the threshold is at two thirds
    if band_no == "B2":
        threshold = 800 ## the threshold for B2
    else:
        threshold = 2600 ## the threshold for B5
    ## FIXME
    difference_binarized = difference.copy()
    difference_binarized[difference <= threshold] = 1
    difference_binarized[difference > threshold] = 0

    difference_thresh = difference.copy()
    difference_thresh[difference < threshold] = 0


    path_to_tif_file = Path(f"binarized_change_{band_no}.tif")
    with rasterio.open(path_to_tif_file, 'w', **profile) as dst:
        dst.write(difference_binarized.astype(rasterio.float64), 1)

    fig, ax = plt.subplots(1, figsize=(12, 12))
    ax.set_title(f'Differenced Image Binarized After Thresholding for band {band_no}')
    plt.imshow(difference_binarized, cmap='Greys_r')
    plt.savefig(f"image_differencing_binarized_{band_no}.png")
    plt.show(cmap='Greys_r', ax=ax)

    path_to_tif_file = Path(f"after_thresholding_change_{band_no}.tif")
    with rasterio.open(path_to_tif_file, 'w', **profile) as dst:
        dst.write(difference_thresh.astype(rasterio.float64), 1)

    fig, ax = plt.subplots(1, figsize=(12, 12))
    ax.set_title(f'Differenced Image After Thresholding for band {band_no}')
    plt.imshow(difference_thresh, cmap='Greys_r')
    plt.savefig(f"image_differencing_{band_no}.png")
    plt.show(cmap='Greys_r', ax=ax)
