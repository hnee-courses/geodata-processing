from pathlib import Path

import rasterio
from loguru import logger
import rasterio
import pandas as pd
import numpy as np
from rasterio.plot import show_hist
import seaborn as sns
import matplotlib.pyplot as plt


def plot_histograms(image_to_match, filename, title, max_y = None):
    """

    :param image_to_match:
    :param filename:
    :param title:
    :return:
    """
    nd_band_i = image_to_match

    nd_band_i_flat = nd_band_i.ravel()
    plot_band_i = sns.histplot(
        data=nd_band_i_flat, kde=False, bins=1000)
    plot_band_i.set_title(f"{title}")
    plot_band_i.set_ylabel("Frequency", fontsize = 10)
    plot_band_i.set_xlabel("Pixel Value", fontsize = 10)
    fig = plot_band_i.get_figure()
    if max_y is not None:
        plt.ylim(0, max_y)
    plt.show()
    fig.savefig(Path(f"{filename}"))


def histogram_matching(image_to_match_path, reference_image_path, output_path):
    """

    :param image_to_match_path:
    :param reference_image_path:
    :return:
    """

    import matplotlib.pyplot as plt

    from skimage import data
    from skimage import exposure
    from skimage.exposure import match_histograms

    #reference = data.coffee()
    #image = data.chelsea()

    dataset_to_match = rasterio.open(image_to_match_path)

    profile = dataset_to_match.profile
    image_to_match = dataset_to_match.read(1)
    plot_histograms(image_to_match, title="Image to Match", filename="band_05/image_to_match.png")

    dataset_reference = rasterio.open(reference_image_path)
    reference_image = dataset_reference.read(1)
    plot_histograms(reference_image, title="Reference Image", filename="band_02/reference_image.png")

    matched = match_histograms(image_to_match, reference_image, channel_axis=-1)
    plot_histograms(matched, title="Matchted Image", filename="band_05/matched_image.png")

    fig, (ax1, ax2, ax3) = plt.subplots(nrows=1, ncols=3, figsize=(8, 3),
                                        sharex=True, sharey=True)
    for aa in (ax1, ax2, ax3):
        aa.set_axis_off()

    ax1.imshow(image_to_match)
    ax1.set_title('Source')
    ax2.imshow(reference_image)
    ax2.set_title('Reference')
    ax3.imshow(matched)
    ax3.set_title('Matched')


    plt.tight_layout()
    plt.savefig(f"matched_images_comparison.png")
    plt.show()

    path_to_tif_file = Path(output_path)
    with rasterio.open(path_to_tif_file, 'w', **profile) as dst:
        dst.write(matched.astype(rasterio.uint16), 1)
    # matched.rio.to_raster(path_to_tif_file, driver="GTiff")
    logger.info(f"wrote raster to {path_to_tif_file}")

    return path_to_tif_file