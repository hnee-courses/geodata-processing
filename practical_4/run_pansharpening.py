import otbApplication

def run_pansharpening(image_path):


    app = otbApplication.Registry.CreateApplication("Pansharpening")

    app.SetParameterString("inp", "QB_Toulouse_Ortho_PAN.tif")
    app.SetParameterString("inxs", "QB_Toulouse_Ortho_XS.tif")
    app.SetParameterString("out", "Pansharpening.tif")
    app.SetParameterOutputImagePixelType("out", 3)

    app.ExecuteAndWriteOutput()