from pathlib import Path

from loguru import logger

from practical_2.clip_imge_by_aoi import clip_raster_with_aoi, clip_all_matching_images
from practical_2.task_1_get_image_statistics import dark_object_correction
from practical_4.histogram_matching import histogram_matching
from practical_4.run_image_differencing import run_image_differencing



if __name__ == '__main__':
    """
    """
    aoi_path = Path("F:\\QGIS_projects\\Geodataprocessing - Practical 4\\AOI\\30km_square.shp")
    ## slice images to AOI
    band_no = "B2"
    raster_2017 = Path(
        f"F:\\QGIS_projects\\Geodataprocessing - Practical 4\\new_collection\\LC08_L1TP_159069_20170706_20200903_02_T1")
    raster_2021 = Path(f"F:\\QGIS_projects\\Geodataprocessing - Practical 4\\new_collection\\LC08_L1TP_159069_20210717_20210729_02_T1")

    CLIP = False
    if CLIP:
        gen_clips = clip_all_matching_images(aoi_path=aoi_path, template="LC08*.tif", raster_base_path=raster_2017)
        for clip in gen_clips:
            logger.info(f"clipped raster to AOI: {clip}")

        gen_clips = clip_all_matching_images(aoi_path=aoi_path, template="LC08*.tif", raster_base_path=raster_2021)
        for clip in gen_clips:
            logger.info(f"clipped raster to AOI: {clip}")

    image_to_match = f"clip_LC08_L1TP_159069_20210717_20210729_02_T1_{band_no}.TIF"
    image_2017_band_a = raster_2017.joinpath(f"clip_LC08_L1TP_159069_20170706_20200903_02_T1_{band_no}.TIF")
    image_2021_band_a = raster_2021.joinpath(image_to_match)


    ## create the path for the matched image
    matched_image_2021_band_a = raster_2021.joinpath(f"matched_{image_to_match}")
    matched_image_2021_band_a = histogram_matching(
        image_to_match_path=image_2021_band_a,
        reference_image_path=image_2017_band_a,
        output_path= Path(f"{matched_image_2021_band_a}")
    )

    ## TODO remove thin clouds
    ### This does not work. Because those clouds are not as visible on the other bands
    ### And for each other band this would be different
    # raster_2017_band_9 = raster_2017.joinpath("clip_LC08_L1TP_159069_20170706_20200903_02_T1_B9.TIF")
    # raster_2021_band_9 = raster_2021.joinpath("clip_LC08_L1TP_159069_20210717_20210729_02_T1_B9.TIF")
    #
    # image_2017_band_a = run_image_differencing(
    #     a_path=image_2017_band_a,
    #     b_path=raster_2017_band_9,
    #     output_path="clip_LC08_L1TP_159069_20170706_20200903_02_T1_B9.TIF",
    #     vis_output_path="image_2017_thin_cloud"
    # )
    #
    # image_2021_band_a = run_image_differencing(
    #     a_path=matched_image_2021_band_a,
    #     b_path=raster_2021_band_9,
    #     output_path="clip_LC08_L1TP_159069_20170706_20200903_02_T1_B9.TIF",
    #     vis_output_path="image_2021_thin_cloud"
    # )

    image_2021_band_a = dark_object_correction(matched_image_2021_band_a,
                                               band_no=band_no, output_path=f"2021_dark_object_band_{band_no}.tif")
    image_2017_band_a = dark_object_correction(image_2017_band_a,
                                               band_no=band_no, output_path=f"2017_dark_object_band_{band_no}.tif")


    ## TODO implement the pansharpening. Installing this via conda in linux should work. Windows sucks again.
    ### https://www.orfeo-toolbox.org/CookBook/Installation.html#conda-package
    # pan_sharpenend_image_2017_band_a = run_pansharpening(image_2017_band_a)
    # pan_sharpenend_matched_image_2021_band_a = run_pansharpening(matched_image_2021_band_a)

    ## TODO Perform Image Differencing between the two images
    difference_image_outputpath = run_image_differencing(
        a_path=image_2021_band_a,
        b_path=image_2017_band_a,
        output_path=f"image_differencing_2017_2021_{band_no}.tif",
        vis_output_path=f"2017_2021_{band_no}",
        band_no=band_no
    )



