import pandas as pd
from loguru import logger

class_id_mapping = {
2: 'Building', 3: 'Road', 4: 'Tree', 5:'Grassland', 6:'Baresoil', 7:'Water Surface', 8:'Cloud'
}

def confusion_matrix(train_test_df):
    ## build the confusion matrix
    from sklearn.metrics import confusion_matrix
    import matplotlib.pyplot as plt

    #    y_true = [2, 0, 2, 2, 0, 1]
    #    y_pred = [0, 0, 2, 2, 0, 2]
    y_pred = train_test_df["classified"]
    y_true = train_test_df["reference"]
    cf_matrix = confusion_matrix(y_true, y_pred)

    import seaborn as sns

    ax = sns.heatmap(cf_matrix, annot=True, cmap='Blues')

    ax.set_title('Confusion Matrix of Pixel Classification \n')
    ax.set_xlabel('Classified Values')
    ax.set_ylabel('Reference Values ')

    ## Ticket labels - List must be in alphabetical order

    # ax.xaxis.set_ticklabels(['False','True'])
    classes = ['Building', 'Road', 'Tree', 'Grassland', 'Baresoil', 'Water Surface', 'Cloud']
    ax.xaxis.set_ticklabels(classes, rotation=25)
    # ax.yaxis.set_ticklabels(['False','True'])
    ax.yaxis.set_ticklabels(classes, rotation=65)

    ## Display the visualization of the Confusion Matrix.
    plt.tight_layout()
    plt.savefig("confusion_matrix.png")
    plt.show()


    return cf_matrix


def accuracy_measurements(train_test_df, class_id_mapping):
    """
    A reference-based accuracy.
        Producer accuracy (forest) = (# of pixels correctly classified as
        forest) / (# ground reference pixels in forest) = 900/900 = 100%
    :param class_id_mapping:
    :return:
    """
    total_correctly_classified_pixels = 0
    total_pixels = len(train_test_df)
    result_all = {}
    for key, value in class_id_mapping.items():
        # print(value)
        result = {}
        df_all_reference = train_test_df[train_test_df["reference"] == key ]
        df_correctly_classified_pixels = df_all_reference[df_all_reference["classified"] == key ]
        df_all_classified = train_test_df[train_test_df["classified"] == key ]

        ground_reference_pixels = len(df_all_reference)
        correctly_classified = len(df_correctly_classified_pixels)
        total_correctly_classified_pixels += correctly_classified

        producer_accuracy = round(correctly_classified/ground_reference_pixels, 3)
        logger.info(f"producer accuracy for {value}: {producer_accuracy} ")
        result["producer_accuracy"] = producer_accuracy

        user_accuracy = round(correctly_classified/len(df_all_classified), 3)
        logger.info(f"user accuracy for {value}: {user_accuracy} ")
        result["user_accuracy"] = user_accuracy

        result_all[value] = result

    df_individual_stats = pd.DataFrame(result_all).T
    overall_accuracy = total_correctly_classified_pixels/total_pixels
    logger.info(f"Overall accuracy: {overall_accuracy}")

    return df_individual_stats, overall_accuracy

def kappa_equation(train_test_df, cf):
    """

    :param train_test_df:
    :param cf:
    :return:
    """
    N = len(train_test_df)
    total_reference_row_total = {}
    correctly_classified = 0
    chance_agreement = 0
    for i in range(cf.shape[0]):
        total_reference_row_total[i] = sum(cf[i,:])
        correctly_classified += cf[i,i] # the matrix is symetric
        chance_agreement += sum(cf[i, :] * cf[:, i])

    k = ( N * correctly_classified - chance_agreement ) / ( N**2 - chance_agreement)
    logger.info(f"kappa: {k}")

    return k



def run_statistics(train_test_df):
    cf = confusion_matrix(train_test_df)

    df_individual_stats, overall_accuracy = accuracy_measurements(train_test_df, class_id_mapping)

    kappa = kappa_equation(train_test_df, cf)

    return df_individual_stats, overall_accuracy, kappa



if __name__ == '__main__':
    with open("classification_ground_truth.csv", 'r') as f:
        df_gt = pd.read_csv(f)

    df_individual_stats, overall_accuracy, kappa = run_statistics(df_gt)
    print(df_individual_stats)
    with open("individual_stats.csv", 'w') as f:
        df_individual_stats.to_csv(f)