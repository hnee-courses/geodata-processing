"""
process a single image and execute pixel wise landcover classification
"""
from pathlib import Path

from practical_2.clip_imge_by_aoi import clip_raster_with_aoi


def practical_3_part_1(raster_base_path):
    """
    execute supervised landcover classification

    :param raster_base_path:
    :return:
    """

    ## clip the image



    rasters = raster_base_path.glob("T33UUU*.jp2")
    # raster_path =  raster_base_path.joinpath("LC08_L1TP_193023_20220515_20220515_02_RT_B1.TIF")
    aoi_path = Path("F:\QGIS_projects\Geodataprocessing - Practical 2\\aoi.shp")


    for raster in rasters:
        parts = list(raster.parts)
        parts[-1] = f"cut_{parts[-1]}"
        clip_raster_with_aoi(raster_path=raster, aoi=aoi_path, output_path=Path(*parts))
    ## define a training data set

    ##


if __name__ == '__main__':
    raster_base_path = Path(
        "F:\QGIS_projects\Geodataprocessing - Practical 2\Sentinel\S2B_MSIL1C_20220307T101759_N0400_R065_T33UUU_20220307T122828.SAFE\GRANULE\L1C_T33UUU_A026117_20220307T102247\IMG_DATA")

    practical_3_part_1(raster_base_path)

    ## TODO: https://ceholden.github.io/open-geo-tutorial/python/chapter_5_classification.html
    ## https://medium.com/analytics-vidhya/land-cover-classification-97e9a1c77444